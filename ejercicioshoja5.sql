﻿﻿USE ciclistas;

-- 1 ejercicio

  -- c1
  SELECT DISTINCT e.dorsal  FROM etapa e;

  --  final
  SELECT c.nombre, c.edad FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 2 ejercicio

  -- c1
  SELECT DISTINCT p.dorsal 
    FROM puerto p;

  -- final
  SELECT c.nombre, c.edad  FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 3 ejercicio

  -- c1
  SELECT DISTINCT c.nomequipo   FROM ciclista c   LEFT JOIN etapa e1  ON c.dorsal = e1.dorsal   WHERE e1.dorsal IS NULL;

  -- Final
  SELECT e.director  FROM equipo e 
    JOIN (
      SELECT DISTINCT c.nomequipo 
        FROM ciclista c 
        LEFT JOIN etapa e1 
        ON c.dorsal = e1.dorsal 
        WHERE e1.dorsal IS NULL
    ) c1 
    ON e.nomequipo = c1.nomequipo; 

-- 4 ejercicio

  -- c1
  SELECT DISTINCT l.dorsal FROM lleva l;

  -- final
  SELECT c.dorsal, c.nombre FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT l.dorsal 
        FROM lleva l
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 5 ejercicio

  -- c1
  SELECT DISTINCT c.dorsal  FROM ciclista c  JOIN lleva l  ON c.dorsal = l.dorsa  WHERE l.código='MGE';

  -- final 
  SELECT c.dorsal, c.nombre  FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT c.dorsal 
        FROM ciclista c 
        JOIN lleva l 
        ON c.dorsal = l.dorsal 
        WHERE l.código='MGE'
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 6 ejercicio

  -- c1
  SELECT DISTINCT p.numetapa FROM puerto p;

  -- final
  SELECT e.numetapa  FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 7 ejercicios

  -- c1
  SELECT DISTINCT p.numetapa FROM puerto p;

  -- final
  SELECT AVG(e.kms)distanciaMedia  FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 8 ejercicio

  -- c1
  SELECT DISTINCT e.dorsal FROM etapa e;

  --  final
  SELECT COUNT(*) numCiclistas FROM ciclista c 
    LEFT JOIN (
      SELECT DISTINCT e.dorsal 
        FROM etapa e
    ) c1 
    ON c.dorsal = c1.dorsal 
    WHERE c1.dorsal IS NULL;

-- 9 ejercicio

  -- c1
  SELECT DISTINCT p.numetapa  FROM puerto p;

  -- final
  SELECT DISTINCT e.dorsal  FROM etapa e 
    LEFT JOIN (
      SELECT DISTINCT p.numetapa 
        FROM puerto p
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;

-- 10 ejercicio

  -- c1
  SELECT e.dorsal, e.numetapa  FROM  etapa e   JOIN puerto p  ON e.numetapa = p.numetapa;

  -- Final
  SELECT DISTINCT e.dorsal  FROM etapa e 
    LEFT JOIN (
      SELECT e.dorsal, e.numetapa 
      FROM  etapa e 
      JOIN puerto p 
      ON e.numetapa = p.numetapa
    ) c1 
    ON e.numetapa = c1.numetapa 
    WHERE c1.numetapa IS NULL;
